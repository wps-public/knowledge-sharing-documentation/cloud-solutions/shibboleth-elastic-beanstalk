# Elastic Beanstalk Platform Extensions

This directory contains example files to extend your Elastic Beanstalk platform. Please review the
[Extending Elastic Beanstalk Linux platforms](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/platforms-linux-extend.html)
documentation before using these scripts and configuration files.

**Amazon Linux 2 Only!** These examples are specific to the **[Python 3.8 Amazon Linux 2 platform](https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platform-history-python.html)**. They will not work on older platforms using Amazon Linux 1. If you use another language stack, you should be able to work off this configuration, so long as Apache is provided as a proxy server.

**Copy and Paste Warning!** Review these scripts and what they do before adding them to your environment.

## Required Customizations

Locations that will require information unique to your application are called out with a `PROVIDE-CONFIG` keyword.

| Configuration Item                                            | Description                                                          |
| ------------------------------------------------------------- | -------------------------------------------------------------------- |
| File: .platform/shibboleth2.xml                               | Shibboleth SP configuration file                                     |
| File: .platform/httpd/vhost.conf                              | Provide path to your app, set up locations Shibboleth should protect |
| Bash Variable: .platform/hooks/prebuild/shibboleth.sh:SUBJECT | Subject for Apache's self-signed certificate                         |
| Environment Variable: SHIB_SECRETS_ARN                        | ARN to a Secrets Manager secret containing Shibboleth secrets        |
| Environment Variable: SHIB_SECRETS_REGION                     | Region where the secret is stored                                    |

## Design Considerations

### WSGI and Gunicorn

In the unmodified platform, the Apache server proxies HTTP requests to a [gunicorn WSGI HTTP server](https://gunicorn.org/). Apache needs to provide the Shibboleth session information to your web application. The most secure method is with environment variables. This requires Apache to directly interact with your application using mod_wsgi.

Gunicorn is no longer required, but must be running for Elastic Beanstalk to consider the service healthy. A `Procfile` is provided to run gunicorn with a "dummy" WSGI application. This does not affect Elastic Beanstalk's monitoring capabilities.

### Self-signed Certificate

The Apache proxy server is intentionally configured with a self-signed SSL certificate. This is to ensure there is SSL encryption end-to-end, which Shibboleth (and your security team) prefer. Only the Application Load Balancer communicates with the proxy server, and it accepts any self-signed certificate.

### Providing Secrets via Secrets Manager

It's important not to commit secrets to source control. It is recommended you store your Shibboleth secrets in AWS Secrets Manager. See [Secure Storage of Shibboleth Secrets](../../docs/shib-secrets.md) for further assistance.

## Troubleshooting

Things will break while you implement these examples. There are many different log files that can help you out. Using the `eb logs` command will get you a selection of logs. To view additional logs, you can use `eb ssh` and navigate to `/var/log`.

| Log File(s)     | Description                            |
| --------------- | -------------------------------------- |
| eb-engine.log   | Start here, main Elastic Beanstalk log |
| eb-hooks.log    | Output from your platform hooks        |
| httpd/error_log | Configuration logs for Apache          |
| shibboleth/\*   | Shibboleth logs                        |

**Debug Logs:** You can configure [Shibboleth logging](https://wiki.shibboleth.net/confluence/display/SP3/Logging) and/or [Apache logging](https://httpd.apache.org/docs/2.4/logs.html) to output DEBUG-level logs as well.

Apache config location: `/etc/httpd/`
Shibboleth config location: `/etc/shibboleth/`

### Broken Apache Config Bug

If you've deployed a broken Apache configuration file, you may have to log into the instance and delete the file(s) before you attempt a deployment. Otherwise, the deployment will error out on the existing bad config. This bug may have been fixed since the time of this writing.
