#!/usr/bin/env bash
# Installs shibboleth and retrieves secrets

set -e

echo "Checking if Shibboleth is installed..."
if ! yum list installed shibboleth; then
  echo "Installing shibboleth..."

  cat <<EOF > /etc/yum.repos.d/shibboleth.repo
[shibboleth]
name=Shibboleth (amazonlinux2)
# Please report any problems to https://shibboleth.atlassian.net/jira
type=rpm-md
mirrorlist=https://shibboleth.net/cgi-bin/mirrorlist.cgi/amazonlinux2
gpgcheck=1
gpgkey=https://shibboleth.net/downloads/service-provider/RPMS/repomd.xml.key
        https://shibboleth.net/downloads/service-provider/RPMS/cantor.repomd.xml.key
enabled=1
EOF

  yum update
  yum -y install shibboleth.x86_64

  # Remove pre-generated keys and certs
  rm /etc/shibboleth/*.pem
else
  echo "Shibboleth is already installed"
fi

source .platform/common/shib_copy_config.sh
source .platform/common/shib_retrieve_secrets.sh
source .platform/common/shib_restart.sh
