#!/usr/bin/env bash
# Install WSGI support for Apache so that it can pass shibboleth session data to the application.
# mod_wsgi must be installed from pip in order to compile against Python 3. Don't use Amazon Linux's mod_wsgi package,
# as it is compiled for the system Python 2.

set -e

if [[ -z "${PYTHONPATH}" ]]; then
    echo "ERROR: PYTHONPATH not defined, aborting WSGI installation"
fi

source "${PYTHONPATH}/activate"
pip install mod_wsgi
mod_wsgi-express install-module > /etc/httpd/conf.modules.d/wsgi.conf

# Pass environment variables set by Elastic Beanstalk when systemd starts Apache
systemctl import-environment
