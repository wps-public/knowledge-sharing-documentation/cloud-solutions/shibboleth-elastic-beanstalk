This directory contains code that is common between the "confighooks" and "hooks" scripts. It is not part of the Elastic Beanstalk Platform Hooks convention.
