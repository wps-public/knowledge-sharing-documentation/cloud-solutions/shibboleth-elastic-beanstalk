# Secure Storage of Shibboleth Secrets

Shibboleth secrets should be stored in [Amazon Secrets Manager](https://docs.aws.amazon.com/secretsmanager/latest/userguide/intro.html). Secrets Manager helps protect secrets with secure storage and retrieval.

[An example script](../examples/elastic-beanstalk/.platform/common/shib_retrieve_secrets.sh) is provided to retrieve Shibboleth secrets and store them where Shibboleth expects them. This document explains how to use the script.

## Storing Secrets

[Create a secret](https://docs.aws.amazon.com/secretsmanager/latest/userguide/manage_create-basic-secret.html) with type: "Other type of secrets". The script is designed to read secrets stored in a JSON object. The keys are the secrets' file names, and the values are the files' contents. Here is an example:

```json
{
  "sp-encrypt-cert.pem": "-----BEGIN CERTIFICATE-----\n ...",
  "sp-encrypt-key.pem": "-----BEGIN PRIVATE KEY-----\n...",
  "sp-signing-cert.pem": "-----BEGIN CERTIFICATE-----\n ...",
  "sp-signing-key.pem": "-----BEGIN PRIVATE KEY-----\n ...",
  "login.wisc.edu-signing.pem": "-----BEGIN CERTIFICATE-----\n ..."
}
```

**Newline Encoding:** Newlines must be replaced with the `\n` character. The certificates and keys are [Base64 encoded](https://en.wikipedia.org/wiki/Base64), so line lengths must be preserved. JSON does not permit multi-line strings.

Minimally, only the private keys need to be stored. However, it is convenient to store the public certificates with their keys in one central location. The script's design assumes all Shibboleth certificates and keys will come from Amazon Secrets Manager.

## Retrieving Secrets

### Instance Profile Permissions Policy

EB environments use an [IAM role](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles.html) to access AWS resources, also called an [instance profile](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/iam-instanceprofile.html). By default, this is `aws-elasticbeanstalk-ec2-role`. An [IAM permissions policy](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies.html) must be created to grant access to the secret, then assigned to the instance profile role. **Preferably, [a custom role](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/iam-instanceprofile.html#iam-instanceprofile-create) should be created for the environment.**

#### Example IAM Resources

**Policy to read the secret:**

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "ShibbolethSecretsRead",
      "Effect": "Allow",
      "Action": [
        "secretsmanager:GetSecretValue",
        "secretsmanager:DescribeSecret",
        "secretsmanager:ListSecretVersionIds"
      ],
      "Resource": "arn:aws:secretsmanager:your-secrets-arn"
    }
  ]
}
```

**Custom EB Role and Attached Policies:**

```json
{
  "Role": {
    "RoleName": "myapp-elasticbeanstalk-ec2-role",
    "RoleId": "*********",
    "Arn": "arn:aws:iam::*********",
    "Description": "Identical to aws-elasticbeanstalk-ec2-role with specific permissions for this application.  Allows EC2 instances to call AWS services on your behalf.",
    "MaxSessionDuration": 3600
  },
  "AttachedPolicies": [
    {
      "PolicyName": "AWSElasticBeanstalkWebTier",
      "PolicyArn": "arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier"
    },
    {
      "PolicyName": "ShibbolethSecretsRead",
      "PolicyArn": "arn:aws:iam::******"
    }
  ]
}
```

Remember to [assign the role as the instance profile](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/using-features.managing.security.html#using-features.managing.security.console) of the Elastic Beanstalk environment.

### Required Environment Variables

`SHIB_SECRETS_ARN` and `SHIB_SECRETS_REGION` must be defined for the script to operate. See [Examples README](../examples/elastic-beanstalk/README.md) for details.

### Script Location

The example script can be found at: [examples/elastic-beanstalk/.platform/common/shib_retrieve_secrets.sh](../examples/elastic-beanstalk/.platform/common/shib_retrieve_secrets.sh)
